#Weapon classes
class Weapon:
    def __init__(self):
        raise NotImplementedError("Do not create raw Weapon objects.")

    def __str__(self):
        return self.name

class Rock(Weapon):
    def __init__(self):
        self.name = "Rock"
        self.description = "A fist-sized rock, suitable for bludgeoning."
        self.damage = 5

class Dagger(Weapon):
    def __init__(self):
        self.name = "Dagger"
        self.description = "A small dagger with some rust. Somewhat more dangerous than a rock."
        self.damage = 10

class RustySword(Weapon):
    def __init__(self):
        self.name = "Rusty Sword"
        self.description = "This sword is showing its age, but it still has some fight in it."
        self.damage = 20

class RustyAxe(Weapon):
    def __init__(self):
        self.name = "Rusty Axe"
        self.description = "You can't tell if it is dried blood or rust, but it should still do the job."
        self.damage = 25

def play():
    #Inventory list
    inventory = [Rock(), Dagger(), 'Gold(5)', 'Crusty Bread']

    #Title
    print("Escape from Cave Terror!")

    #Game loop
    while True:
        #Action input
        action_input = get_player_command()
        action_input = action_input.title()

        #Check input
        if action_input == 'N':
            print("Go North!")
        elif action_input == 'S':
            print("Go South!")
        elif action_input == 'E':
            print("Go East!")
        elif action_input == 'W':
            print("Go West!")
        elif action_input == 'I':
            print("Inventory:")
            for item in inventory:
                print("* " + str(item))
        else:
            print("Invalid action!")

def get_player_command():
    return input('Action: ')

def most_powerful_weapon(inventory):
    max_damage = 0
    best_weapon = None
    for item in inventory:
        try:
            if item.damage > max_damage:
                best_weapon = item
                max_damage = item.damage
        except AttributeError:
            pass

    return best_weapon

play()
